import json


def Layout(layout):
    def wrapper(wrapped):
        def newmethod(*args, **kwargs):
            req = None
            if len(args) > 1:
                req = args[1]
            else:
                req = args[0]
            d = wrapped(req)
            return dict(layout(req), **d)
        return newmethod
    return wrapper


def jsonobj(wrapped):
    def wrapper(request):
        jsonobj = {}
        if request.GET and request.GET['data'] > 0:
            jsonobj = json.loads(request.GET['data'])
        else:
            jsonobj = request.json_body

        return wrapped(request, jsonobj)
    return wrapper


def webparams(wrapped):
    def wrapper(request):
        all = request.matchdict
        return wrapped(request, **all)
    return wrapper


def result(fn=None, success='success', failure='failure'):
    def decorator(fn):
        def decorated(*args, **kwargs):
            r = None
            try:
                r = fn(*args, **kwargs) or {}
            except Exception as e:
                r = {'result': failure, 'exception': e}
            r['result'] = success
            return r
        return decorated
    if fn is None:
        return decorator
    return decorator(fn)
