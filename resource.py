from pyramid.security import authenticated_userid
from pyramid.security import DENY_ALL
from pyramid.events import ContextFound, subscriber
from pyramid.exceptions import NotFound

@subscriber(ContextFound)
def contextFoundSubscriber(event):
    if isinstance(event.request.context, ResourceMixin):
        event.request.context.onContextFound()


class ResourceMixin(object):

    def __init__(self, request):
        super(ResourceMixin, self).__init__()
        self.request = request

    def onContextFound(self):
        raise NotImplementedError("""This needs to be
                                  implemented by implementers""")


class NodeResourceMixin(ResourceMixin):
    resources = {}

    def __getitem__(self, item):
        c = self.resources[item](self.request)
        c.__parent__ = self
        return c


class LeafResourceMixin(ResourceMixin):

    resource = None

    def instance(self, id):
        raise NotImplementedError("""This needs to be implemented for
                                  resources supporting fetching single
                                  instances""")

    def __getitem__(self, id):
        self.resource = self.instance(id)
        if self.resource is None:
            raise NotFound(id)
        return self


class HybridResourceMixin(LeafResourceMixin, NodeResourceMixin):
    resources = {}

    def __getitem__(self, id):
        r = None
        c = None
        try:
            c = self.resources[id](self.request)
            c.__parent__ = self
        except KeyError:
            r = self.instance(id)

        if c is None and r is None:
            raise KeyError
        if r is not None:
            self.resource = r
        return c or self


class OwnedResourceMixin(object):

    def __get_owner(self, r):
        owner = None
        try:
            owner = self.get_owner(r)
        except:
            pass
        return owner

    def get_owner(self, r):
        raise NotImplementedError("Subclasses need to define this method")

    def __acl__(self):
        result_acl = []
        try:
            resource = getattr(self, 'resource', None)
            if resource is not None:
                owner = self.__get_owner(resource)
                if owner is not None:
                    for p, a in self.owner_acl:
                        result_acl.append((p, owner, a))
            result_acl.extend(self.default_acl)
            result_acl.append(DENY_ALL)
        except:
            #Log the error
            result_acl = [DENY_ALL]  # Problem with the configuration, deny everything.
        finally:
            if len(result_acl) is 0:
                result_acl.append(DENY_ALL)  # No permissions means something's fishy.

        return result_acl


class HttpResource(ResourceMixin):

    def __init__(self, request):
        super(HttpResource, self).__init__(request)
        self.body = self.get_body(request)

    def onContextFound(self):
        self._httpmethods[self.request.method](self)

    @staticmethod
    def get_body(req):
        ret = None
        if req.method in ['POST', 'PUT', 'PATCH']:
            ret = req.POST
        else:
            ret = req.body
        return ret

    @staticmethod
    def factory(o):
        return o

    def post(self, obj):
        pass

    def _factory(self, o):
        return self.factory(o)

    def _post(self):
        self.post(self._factory(self.get_body(self.request)))

    def get(self):
        pass

    def _get(self):
        if self.resource is None:
            self.resource = self.collection()
        self.get()

    def put(self, obj):
        pass

    def _put(self):
        self.put(self._factory(self.get_body(self.request)))

    def patch(self, obj):
        pass

    def _patch(self):
        self.patch(self._factory(self.get_body(self.request)))

    def delete(self):
        pass

    def _delete(self):
        self.delete()

    _httpmethods = {
        'POST': _post,
        'GET': _get,
        'PUT': _put,
        'DELETE': _delete,
        'PATCH': _patch
    }


class PyramidAuthMixin(object):

    def __init__(self, request):
        self.user_id = self.get_user_id(request)
        super(PyramidAuthMixin, self).__init__(request)

    def get_user_id(self, req):
        return authenticated_userid(req)


class PyramidJsonHttpResourceMixin(object):

    @staticmethod
    def get_body(req):
        try:
            return req.json_body
        except:
            return None


class KwargsFactoryResourceMixin(object):
    def _factory(self, o):
        try:
            return self.factory(**o)
        except TypeError:
            return self.factory(o)
        except KeyError:
            return None


class PyramidJsonAuthHttpResource(PyramidJsonHttpResourceMixin,
                                  PyramidAuthMixin,
                                  KwargsFactoryResourceMixin, HttpResource):
    pass


class PyramidJsonHttpResource(PyramidJsonHttpResourceMixin,
                              KwargsFactoryResourceMixin, HttpResource):
    pass


class PyramidAuthHttpResource(PyramidAuthMixin, HttpResource):
    pass
