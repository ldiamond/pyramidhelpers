import unittest
import mock
from pyramid.request import Request
from pyramid import testing
from resource import (HttpResource,
                      PyramidJsonAuthHttpResource as pjahr,
                      NodeResourceMixin,
                      LeafResourceMixin,
                      OwnedResourceMixin,
                      HybridResourceMixin,
                      )
from pyramid.security import DENY_ALL, Allow, Authenticated, has_permission, ACLAllowed, ACLDenied
from pyramid.authorization import ACLAuthorizationPolicy
import json
import sys


class HTTPResourceTests(unittest.TestCase):

    def setUp(self):
        self.config = testing.setUp()

        class Resource(HttpResource):
            pass
        self.httpresource = Resource

    def tearDown(self):
        testing.tearDown()

    def test_HttpResource_get(self):
        """ Get does almost nothing
        """
        req = Request.blank('/')
        req.method = 'GET'
        m = mock.Mock()
        self.httpresource.get = m
        self.httpresource(req)
        m.get.assert_called_once()

    def test_HttpResource_post_put_patch(self):
        """ POST
            The post method should be called with
            the form data
        """
        class Req(object):
            method = 'POST'
            POST = 'POTATO'

        m = mock.Mock()
        self.httpresource.post = m
        self.httpresource.put = m
        self.httpresource.patch = m
        req = Req()
        self.httpresource(req)
        req.method = 'PUT'
        req.POST = 'snack'
        self.httpresource(req)
        req.method = 'PATCH'
        req.POST = 'poil'
        self.httpresource(req)
        c = mock.call
        expected = [c('POTATO'), c('snack'), c('poil')]
        assert m.call_args_list == expected

    def test_json_auth(self):
        self.config.testing_securitypolicy(userid='Bob')
        req = Request.blank('/')
        body = {'a': 'Weee', 'B': 'Bewww'}
        req.body = json.dumps(body)
        hr = pjahr(req)
        self.assertEqual(hr.user_id, 'Bob')
        self.assertEqual(hr.body, body)


class ResourceMixinsTests(unittest.TestCase):

    def setUp(self):
        self.config = testing.setUp()

        class Resource(object):
            @staticmethod
            def get(self, id):
                return id

        class Leaf(LeafResourceMixin):
            __name__ = 'leaf'
            _resource = Resource

        class Hybrid(HybridResourceMixin):
            __name__ = 'hybrid'
            resources = {'leaf': Leaf}

        class Owned(OwnedResourceMixin, HybridResourceMixin):
            __name__ = 'owned'
            resources = {'leaf': Leaf}
            owner_acl = [(Allow, 'all')]
            default_acl = [(Allow, Authenticated, 'view')]
            owner = 'Bob'
            resource = None

            def get_owner(self, resource):
                return self.owner

        class Root(NodeResourceMixin):
            __name__ = 'root'
            resources = {'hybrid': Hybrid,
                         'owned': Owned,
                         }

        self.config.testing_securitypolicy(userid='Bob')
        self.config.set_authorization_policy(ACLAuthorizationPolicy())
        self.request = Request.blank('/')
        self.root = Root(self.request)
        self.Hybrid = Hybrid
        self.Leaf = Leaf
        self.Root = Root
        self.Owned = Owned

    def test_root_resource(self):
        r = self.root
        h = r['hybrid']
        l = h['leaf']
        o = r['owned']
        ol = o['leaf']

        self.assertIsInstance(h, self.Hybrid)
        self.assertIsInstance(l, self.Leaf)
        self.assertIsInstance(o, self.Owned)
        self.assertListEqual(o.__acl__(), [(Allow, 'Bob', 'all'),
                                           (Allow, Authenticated, 'view'),
                                           DENY_ALL])
        self.assertIsInstance(ol, self.Leaf)
        self.assertIsInstance(self.request.has_permission('all', o),
                              ACLAllowed)
        self.assertIsInstance(self.request.has_permission('all',
                                                          Authenticated),
                              ACLDenied)
        self.assertIsInstance(self.request.has_permission('all',
                                                          ol),
                              ACLAllowed)
        self.assertIsInstance(self.request.has_permission('all',
                                                          l),
                              ACLDenied)


if __name__ == '__main__':
    print(sys.path)
    unittest.main()
